<aside class="main-sidebar">

	 <section class="sidebar">

		<ul class="sidebar-menu">

		<?php

		if($_SESSION["perfil"] == "Administrador"){

			echo '<li class="active">

				<a href="index.php?ruta=inicio">

					<i class="fa fa-home"></i>
					<span>Inicio</span>

				</a>

			</li>

			<li>

				<a href="index.php?ruta=usuarios">

					<i class="fa fa-user"></i>
					<span>Usuarios</span>

				</a>

			</li>';

		}

		if($_SESSION["perfil"] == "Administrador" || $_SESSION["perfil"] == "Especial"){

			echo '<li>

				<a href="index.php?ruta=categorias">

					<i class="fa fa-th"></i>
					<span>Categorías</span>

				</a>

			</li>

			<li>

				<a href="index.php?ruta=productos">

					<i class="fa fa-product-hunt"></i>
					<span>Productos</span>

				</a>

			</li>';

		}

		if($_SESSION["perfil"] == "Administrador" || $_SESSION["perfil"] == "Vendedor"){

			echo '<li>

				<a href="index.php?ruta=clientes">

					<i class="fa fa-users"></i>
					<span>Clientes</span>

				</a>

			</li>
			';

		}


		//////////////////////////////////////////////////////


echo '<li class="treeview">

				<a href="#">

					<i class="fa fa fa-dollar -ul"></i>
					
					<span>Presupuesto</span>
					
					<span class="pull-right-container">
					
						<i class="fa fa-angle-left pull-right"></i>

					</span>

				</a>

				<ul class="treeview-menu">
					
			

				

					
						<li>

						<a href="index.php?ruta=crear-presupuesto">
							
							<i class="fa fa-circle-o"></i>
							<span>Crear presupuesto</span>

						</a>

					</li>

					<li>

				<a href="index.php?ruta=presupuesto-cliente">

					<i class="fa fa-circle-o "></i>
					<span>Presupuestos Clientes</span>

				</a>

			</li>

					';

			

				

				echo '</ul>

			</li>';










		if($_SESSION["perfil"] == "Administrador" || $_SESSION["perfil"] == "Vendedor"){

			echo '<li class="treeview">

				<a href="#">

					<i class="fa fa-list-ul"></i>
					
					<span>Ventas</span>
					
					<span class="pull-right-container">
					
						<i class="fa fa-angle-left pull-right"></i>

					</span>

				</a>

				<ul class="treeview-menu">
					
					<li>

						<a href="index.php?ruta=ventas">
							
							<i class="fa fa-circle-o"></i>
							<span>Ventas contado</span>

						</a>

					</li>

					<li>

						<a href="index.php?ruta=ventas-corrientes">
							
							<i class="fa fa-circle-o"></i>
							<span>Ventas corrientes</span>

						</a>

					</li>

					<li>

						<a href="index.php?ruta=crear-venta">
							
							<i class="fa fa-circle-o"></i>
							<span>Crear venta</span>

						</a>

					</li>
						<li>

						<a href="index.php?ruta=crear-venta-corriente">
							
							<i class="fa fa-circle-o"></i>
							<span>Crear venta corriente</span>

						</a>

					</li>
				

					

					';

					if($_SESSION["perfil"] == "Administrador"){

					echo '<li>

						<a href="index.php?ruta=reportes">
							
							<i class="fa fa-circle-o"></i>
							<span>Reporte ventas ctdo</span>

						</a>

					</li>
					<li>

						<a href="index.php?ruta=reportesC">
							
							<i class="fa fa-circle-o"></i>
							<span>Reporte ventas cte</span>

						</a>

					</li>


					';

					}

				

				echo '</ul>

			</li>';

		}

		?>

		</ul>

	 </section>

</aside>