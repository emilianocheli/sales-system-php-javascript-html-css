<?php

require_once "../../../controladores/ventas.controlador.php";
require_once "../../../modelos/ventas.modelo.php";

require_once "../../../controladores/clientes.controlador.php";
require_once "../../../modelos/clientes.modelo.php";

require_once "../../../controladores/usuarios.controlador.php";
require_once "../../../modelos/usuarios.modelo.php";

require_once "../../../controladores/productos.controlador.php";
require_once "../../../modelos/productos.modelo.php";

class imprimirFactura{

public $codigo;

public function traerImpresionFactura(){

//TRAEMOS LA INFORMACIÓN DE LA VENTA

$itemVenta = "codigo";
$valorVenta = $this->codigo;
	$tabla = "rec";


//$ven3 = ControladorVentas::ctrSumaTotalVentas( );











$respuestaVenta = ControladorVentas::ctrMostrarVentas($itemVenta, $valorVenta, "corriente");

$fecha = substr($respuestaVenta["fecha"],0,-8);
$productos = json_decode($respuestaVenta["productos"], true);
$neto = number_format($respuestaVenta["neto"],2);
$impuesto = number_format($respuestaVenta["impuesto"],2);
$total = number_format($respuestaVenta["total"],2);
$cobrado = number_format($respuestaVenta["total_cobrado"],2);



//$ven = number_format($ven3["total"],2);


//TRAEMOS LA INFORMACIÓN DEL CLIENTE

$itemCliente = "id";
$valorCliente = $respuestaVenta["id_cliente"];
$ventas = ControladorVentas::ctrSumaTotalRecibo( "corriente", $valorCliente);

$res = ControladorVentas::ctrMostrarVentas2($valorCliente, $valorCliente, "corriente");
//var_dump($res);


//$resp = ControladorVentas::ctrMostrarVentas($respuestaVenta["id_cliente"],"id_cliente", "corriente");



$itemVenta2= "codigo";
$valorVenta2 = $this->codigo;


$total3 = number_format($ventas["a"],2);

$respuestaCliente = ControladorClientes::ctrMostrarClientes($itemCliente, $valorCliente);


//var_dump($concatenacion);

//TRAEMOS LA INFORMACIÓN DEL VENDEDOR

$itemVendedor = "id";
$valorVendedor = $respuestaVenta["id_vendedor"];

$respuestaVendedor = ControladorUsuarios::ctrMostrarUsuarios($itemVendedor, $valorVendedor);


//REQUERIMOS LA CLASE TCPDF

require_once('tcpdf_include.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->startPageGroup();

$pdf->AddPage();

// ---------------------------------------------------------

$bloque1 = <<<EOF

	<table>

		<tr>

			<td style="width:150px"><img src="images/logo-r.jpg"></td>

			<td style="background-color:white; width:140px">

				<div style="font-size:8.5px; text-align:right; line-height:15px;">

					<br>
					NIT: Sanitarios Rivadavia
					$total3

					<br>
					Dirección: Rivadavia 83


				</div>

			</td>

			<td style="background-color:white; width:140px">

				<div style="font-size:8.5px; text-align:right; line-height:15px;">

					<br>
					Teléfono: (02292) 451608

					<br>
					Celular: 02281539875

				</div>

			</td>

			<td style="background-color:white; width:110px; text-align:center; color:red"><br><br>CTA CORRIENTE</td>

		</tr>

	</table>

EOF;

$pdf->writeHTML($bloque1, false, false, false, false, '');

// ---------------------------------------------------------

$bloque2 = <<<EOF

	<table>

		<tr>

			<td style="width:540px"><img src="images/back.jpg"></td>

		</tr>

	</table>

	<table style="font-size:10px; padding:5px 10px;">

		<tr>

			<td style="border: 1px solid #666; background-color:white; width:540px">

				Cliente: $respuestaCliente[nombre]

			</td>

	
		</tr>


		<tr>

		<td style="border-bottom: 1px solid #666; background-color:white; width:540px"></td>

		</tr>

	</table>

EOF;

$pdf->writeHTML($bloque2, false, false, false, false, '');

// ---------------------------------------------------------

$bloque3 = <<<EOF

	<table style="font-size:10px; padding:5px 10px;">

		<tr>

		<td style="border: 1px solid #666; background-color:white; width:130px; text-align:center">FACTURA N</td>

		<td style="border: 1px solid #666; background-color:white; width:130px; text-align:center">FECHA</td>
		<td style="border: 1px solid #666; background-color:white; width:80px; text-align:center">TOTAL</td>
		<td style="border: 1px solid #666; background-color:white; width:100px; text-align:center">TOTAL COBRADO</td>
		<td style="border: 1px solid #666; background-color:white; width:100px; text-align:center"> DEBE </td>

		</tr>

	</table>

EOF;

$pdf->writeHTML($bloque3, false, false, false, false, '');

// ---------------------------------------------------------

	foreach ($res as $key => $value) {

$product = json_decode($value["productos"], true);

		$fecha = substr($value["fecha"],0,-8);

		$total = number_format($value["total"],2);
		$cobrado = number_format($value["total_cobrado"],2);
$debe = number_format($value["a_cobrar"],2);


$itemProducto = "descripcion";
$valorProducto = $value["codigo"];
$orden = null;

$respuestaProducto = ControladorProductos::ctrMostrarProductos($itemProducto, $valorProducto, $orden);

$valorUnitario = number_format($respuestaProducto["precio_venta"], 2);

$precioTotal = number_format($item["total"], 2);

$bloque4 = <<<EOF

	<table style="font-size:10px; padding:5px 10px;">

		<tr>

			
			<td style="border: 1px solid #666; color:#333; background-color:white; width:130px; text-align:center">
			$valorProducto
			</td>
		    	<td style="border: 1px solid #666; color:#333; background-color:white; width:130px; text-align:center">
		       $fecha		
		   	</td>



			<td style="border: 1px solid #666; color:#333; background-color:white; width:80px; text-align:center">
		
           $$total
			 </td>

			<td style="border: 1px solid #666; color:#333; background-color:white; width:100px; text-align:center">
			$$cobrado
				
			</td>

			<td style="border: 1px solid #666; color:#333; background-color:white; width:100px; text-align:center">
			$$debe
				
			</td>


		</tr>



	</table>


EOF;

$pdf->writeHTML($bloque4, false, false, false, false, '');

}

// ---------------------------------------------------------

$bloque5 = <<<EOF

	<table style="font-size:10px; padding:5px 10px;">

		<tr>

			<td style="color:#333; background-color:white; width:340px; text-align:center"></td>

			<td style="border-bottom: 1px solid #666; background-color:white; width:100px; text-align:center"></td>

			<td style="border-bottom: 1px solid #666; color:#333; background-color:white; width:100px; text-align:center"></td>

		</tr>




		<tr>

			<td style="border-right: 1px solid #666; color:#333; background-color:white; width:340px; text-align:center"></td>

			<td style="border: 1px solid #666; background-color:white; width:100px; text-align:center">
				Total a Pagar:
			</td>

			<td style="border: 1px solid #666; color:#333; background-color:white; width:100px; text-align:center">
				$ $total3
			</td>

		</tr>


	</table>

EOF;

$pdf->writeHTML($bloque5, false, false, false, false, '');



// ---------------------------------------------------------
//SALIDA DEL ARCHIVO
ob_end_clean();

$pdf->Output('corriente.pdf');

}

}

$factura = new imprimirFactura();
$factura -> codigo = $_GET["codigo"];
$factura -> traerImpresionFactura();

?>
