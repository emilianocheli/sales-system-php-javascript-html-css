<?php

require_once "../../../controladores/ventas.controlador.php";
require_once "../../../modelos/ventas.modelo.php";

require_once "../../../controladores/clientes.controlador.php";
require_once "../../../modelos/clientes.modelo.php";

require_once "../../../controladores/usuarios.controlador.php";
require_once "../../../modelos/usuarios.modelo.php";

require_once "../../../controladores/productos.controlador.php";
require_once "../../../modelos/productos.modelo.php";

class imprimirFactura{

public $codigo;

public function traerImpresionFactura(){

//TRAEMOS LA INFORMACIÓN DE LA VENTA

$itemVenta = "codigo";
	//$itemVenta = "id_cliente";
$valorVenta = $this->codigo;

$respuestaVenta  = ControladorVentas::ctrMostrarVentas($itemVenta, $valorVenta,"rec");


$itemVenta2 = null;
$valorVenta2 = null;


$debes = number_format($respuestaVenta2["id"],2);
$valorCliente2 = $respuestaVenta2["id_cliente"];


$fecha = substr($respuestaVenta["fecha"],0,-8);
$productos = json_decode($respuestaVenta["productos"], true);
$neto = number_format($respuestaVenta["neto"],2);
$impuesto = number_format($respuestaVenta["impuesto"],2);
$total = number_format($respuestaVenta["total"],2);
$tiene = number_format($respuestaVenta["total_cobrado"],2);
$debe = number_format($respuestaVenta["a_cobrar"],2);
$fechac = substr($respuestaVenta["vto_cobro"],0,-8);

	
		

$idd = number_format($respuestaRecibo["id_cliente"],2);

            $fechaInicial = null;
            $fechaFinal = null;


          $respuest = ControladorVentas::ctrRangoFechasVentas($fechaInicial, $fechaFinal,"rec");
   //$valor2 = $respuest["codigo"];
 // $debe = number_format($respuest["debe"],2);

  

$res= $respuest[codigo];



//TRAEMOS LA INFORMACIÓN DEL CLIENTE

$itemCliente = "id";
$valorCliente = $respuestaVenta["id_cliente"];
$itemVent2 = "id_cliente";


$itemCliente = "id";
$concatenacion= ControladorVentas::ctrMostrarReciibo($itemVent2, $valorCliente,"rec");
$respuestaV2= ControladorVentas::ctrMostrarVentas("id", $valorCliente,"rec");
$respuest2= ControladorVentas::ctrMostrarVentas("id", $valorCliente,"rec");

//$concatenacion = array_merge($conca, $respuestaV2);
//$concatenacion = '<p>Su nombre y apellido es '.$respuestaV.$respuestaV2.'</p>';


 /*foreach ($concatenacion as $key => $value) {
    if (    $respuestaVenta["id_cliente"]=    $value["id_cliente"])


 }*/

var_dump($concatenacion);
//var_dump($respuestaV2);
//var_dump($respuest2);


//$respuestaVenta2= ControladorVentas::ctrMostrarVentas("id_cliente", $valorCliente,"rec");
//

$respuestaCliente = ControladorClientes::ctrMostrarClientes($itemCliente, $valorCliente);

//TRAEMOS LA INFORMACIÓN DEL VENDEDOR

$itemVendedor = "id";
$valorVendedor = $respuestaVenta["id_vendedor"];

$respuestaVendedor = ControladorUsuarios::ctrMostrarUsuarios($itemVendedor, $valorVendedor);


//REQUERIMOS LA CLASE TCPDF

require_once('tcpdf_include.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage('P', 'A7');

//---------------------------------------------------------
 //foreach ($respuest as $key => $value) {2
    //          $valor2 =  $value["codigo"];
                 
////if ($value[codigo]=$valorVenta) {

//ar_dump($respuestaV);

$bloque1 = <<<EOF

<table style="font-size:9px; text-align:center">

	<tr>
		
		<td style="width:160px;">
	
			<div>
			
				Fecha: $fecha
				$respuest

				<br><br>
Debe $debes 				
	
aber $tiene
vencimieetno $fechac
				<br>
				NIT: 71.759.963-9
				<br>
				Dirección: Calle 
  $valor2;       
				<br>
				Teléfono:ssssssss

				<br>
				FACTURA N.$valorVenta
				$respuestaVenta

				<br><br>					
				Cliente: $respuestaCliente[nombre]

				<br>
				Vendedor: $respuestaVendedor[nombre]


				<br>

			</div>

		</td>

	</tr>


</table>

EOF;

$pdf->writeHTML($bloque1, false, false, false, false, '');



// ---------------------------------------------------------


foreach ($productos as $key => $item) {

$valorUnitario = number_format($item["precio"], 2);

$precioTotal = number_format($item["total"], 2);

$bloque2 = <<<EOF

<table style="font-size:9px;">

	<tr>
	
		<td style="width:160px; text-align:left">
		$item[descripcion] 
		</td>

	</tr>

	<tr>
	
		<td style="width:160px; text-align:right">
		$ $valorUnitario Und * $item[cantidad]  = $ $precioTotal
		<br>
		</td>

	</tr>

</table>

EOF;

$pdf->writeHTML($bloque2, false, false, false, false, '');

}

// ---------------------------------------------------------

$bloque3 = <<<EOF

<table style="font-size:9px; text-align:right">

	<tr>
	
		<td style="width:80px;">
			 NETO: 
		</td>

		<td style="width:80px;">
			$ $neto
		</td>

	</tr>

	<tr>
	
		<td style="width:80px;">
			 IMPUESTO: 
		</td>

		<td style="width:80px;">
			$ $impuesto
		</td>

	</tr>

	<tr>
	
		<td style="width:160px;">
			 --------------------------
		</td>

	</tr>

	<tr>
	
		<td style="width:80px;">
			 TOTAL: 
		</td>

		<td style="width:80px;">
			$ $total
		</td>

	</tr>

	<tr>
	
		<td style="width:160px;">
			<br>
			<br>
			Muchas gracias por su compra
		</td>

	</tr>

</table>



EOF;

$pdf->writeHTML($bloque3, false, false, false, false, '');

// ---------------------------------------------------------
//SALIDA DEL ARCHIVO 

//$pdf->Output('facturaa.pdf', 'D');
$pdf->Output('facturaa.pdf');

}

}

$factura = new imprimirFactura();
$factura -> codigo = $_GET["codigo"];
$factura -> traerImpresionFactura();

?>